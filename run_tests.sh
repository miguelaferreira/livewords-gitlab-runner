#! /bin/bash

set -x
set -e

CONTAINER_NAME=$1
CONTAINER_TEST_IMAGE=$2

docker rm -f ${CONTAINER_NAME} || true

docker run --name ${CONTAINER_NAME} -id ${CONTAINER_TEST_IMAGE}

sleep 1

# tests
docker exec -t ${CONTAINER_NAME} which gitlab-runner
docker stop -t 10 ${CONTAINER_NAME}
docker logs -f ${CONTAINER_NAME} | grep "Shutdown complete"

docker rm -f ${CONTAINER_NAME}
